import model.DataPair;

import java.util.ArrayList;
import java.util.Locale;

public class Main {

    private static final String LEFT = "L";
    private static final String RIGHT = "R";
    private static final String UP = "U";
    private static final String DOWN = "D";

    private static final int AMOUNT = 100;
    private static final int VISITED = 1;
    private static final int MIN_SIZE_MATRIX = 1;
    private static final int NEGATIVE_OFFSET = -1;

    public static void main(String[] args) {
//        ArrayList<DataPair> dataPairs = DataPair.getListRandom(AMOUNT);
        ArrayList<DataPair> dataPairs = DataPair.getListDemo();

        for (DataPair dataPair : dataPairs) {
            String directionFacing = getDirectionFacing(dataPair.getRows(), dataPair.getColumns());
            System.out.println(String.format(Locale.getDefault(), "(%d, %d) - %s", dataPair.getRows(), dataPair.getColumns(), directionFacing));

            String _directionFacing = _getDirectionFacing(dataPair.getRows(), dataPair.getColumns());
            System.out.println(String.format(Locale.getDefault(), "(%d, %d) - %s _", dataPair.getRows(), dataPair.getColumns(), _directionFacing));
        }
    }

    private static String getDirectionFacing(int rows, int columns) {
        String directionFacing;

        if (rows <= columns) {
            directionFacing = (isEven(rows)) ? LEFT : RIGHT;
        } else {
            directionFacing = (isEven(columns)) ? UP: DOWN;
        }

        return directionFacing;
    }

    private static boolean isEven(double num) {
        return ((num % 2) == 0);
    }

    private static String _getDirectionFacing(int rows, int columns) {
        String directionFacing = RIGHT;

        long[][] matrix = new long[rows][columns];
        long matrixSize = rows * columns;

        int n = 0; int m = 0;
        for (long i = 0; i < matrixSize - 1; i++) {
            if (m < columns && isMovingHorizontally(directionFacing)) {
                matrix[n][m] = VISITED;

                if (isMovingRight(directionFacing)) {
                    if ((m + 1) == columns || matrix[n][m + 1] == VISITED) {
                        directionFacing = DOWN;
                        n++;
                    } else {
                        m++;
                    }
                } else {
                    if ((m - 1) == NEGATIVE_OFFSET || matrix[n][m - 1] == VISITED) {
                        directionFacing = UP;
                        n--;
                    } else {
                        m--;
                    }
                }
            } else if (n < rows && isMovingVertically(directionFacing)) {
                matrix[n][m] = VISITED;

                if (isMovingDown(directionFacing)) {
                    if ((n + 1) == rows || matrix[n + 1][m] == VISITED) {
                        m--;
                        directionFacing = LEFT;
                    } else {
                        n++;
                    }
                } else {
                    if ((n - 1) == NEGATIVE_OFFSET || matrix[n - 1][m] == VISITED) {
                        m++;
                        directionFacing = RIGHT;
                    } else {
                        n--;
                    }
                }
            }
        }

        return directionFacing;
    }

    private static boolean isMovingRight(String directionFacing) {
        return directionFacing.equals(RIGHT);
    }

    private static boolean isMovingLeft(String directionFacing) {
        return directionFacing.equals(LEFT);
    }

    private static boolean isMovingUp(String directionFacing) {
        return directionFacing.equals(UP);
    }

    private static boolean isMovingDown(String directionFacing) {
        return directionFacing.equals(DOWN);
    }

    private static boolean isMovingHorizontally(String directionFacing) {
        return directionFacing.equals(RIGHT) || directionFacing.equals(LEFT);
    }

    private static boolean isMovingVertically(String directionFacing) {
        return directionFacing.equals(UP) || directionFacing.equals(DOWN);
    }
}
