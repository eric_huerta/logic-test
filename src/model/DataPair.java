package model;

import java.util.ArrayList;

public class DataPair {

    private static final int MIN = 1;
    private static final int MAX = 5000;

    private static final int MIN_ROW = 1;
    private static final int MAX_ROW = 1000000000;

    private static final int MIN_COLUMNS = 1;
    private static final int MAX_COLUMNS = 1000000000;

    private int rows;
    private int columns;

    public DataPair() {}

    public DataPair(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public int getColumns() {
        return columns;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }

    public static ArrayList<DataPair> getListRandom(int amount) {
        ArrayList<DataPair> dataPairs = new ArrayList<>();

        for (int i = 0; i < amount; i++) {
            if (amount <= DataPair.MAX) {
                int rows = (int) (Math.random() * MAX_ROW) + 1;
                int columns = (int) (Math.random() * MAX_COLUMNS) + 1;

                dataPairs.add(new DataPair(rows, columns));
            } else {
                System.err.println("\nInputs limit exceeded");
            }
        }

        if (amount <= 0) {
            System.err.println("\nInputs minimum is 1");
        }

        return dataPairs;
    }

    public static ArrayList<DataPair> getListDemo() {
        ArrayList<DataPair> dataPairs = new ArrayList<>();

        dataPairs.add(new DataPair(1, 1));
        dataPairs.add(new DataPair(2, 2));
        dataPairs.add(new DataPair(3, 1));
        dataPairs.add(new DataPair(3, 3));
        dataPairs.add(new DataPair(5, 4));
        dataPairs.add(new DataPair(7, 2));
        dataPairs.add(new DataPair(12, 5));
        dataPairs.add(new DataPair(32, 5));
        dataPairs.add(new DataPair(10, 7));
        dataPairs.add(new DataPair(20, 20));
        dataPairs.add(new DataPair(3, 11));
        dataPairs.add(new DataPair(32, 1));
        dataPairs.add(new DataPair(11, 11));
        dataPairs.add(new DataPair(7, 13));
        dataPairs.add(new DataPair(9, 22));
        dataPairs.add(new DataPair(8, 18));

        return dataPairs;
    }
}
